package main

import (
	csv "encoding/csv"
	json "encoding/json"
	"fmt"
	"log"
	"os"
)

type Subscriber struct {
	Email     string `json:"Email"`
	CreatedAt string `json:"Created_at"`
}
type User struct {
	Nick        string       `json:"Nick"`
	Email       string       `json:"Email"`
	CreatedAt   string       `json:"Created_at"`
	Subscribers []Subscriber `json:"Subscribers"`
}

type PathBetween struct {
	ID         int          `json:"id"`
	InitEmail  string       `json:"from"`
	FinalEmail string       `json:"to"`
	Path       []Subscriber `json:"path,omitempty"`
}

func main() {
	resultFileName := "result.json"

	resultFile, err := os.Create(resultFileName)
	if err != nil {
		log.Fatalf("can't create %s %s", resultFileName, err)
	}
	defer resultFile.Close()

	resultEncoder := json.NewEncoder(resultFile)
	resultEncoder.SetIndent("  ", "\t")

	finalAnswer, err := GetFinalAnswer()
	if err != nil {
		log.Fatalf("can't get final answer %s", err)
	}

	err = resultEncoder.Encode(finalAnswer)
	if err != nil {
		log.Fatalf("can't print result %s", err)
	}
}

func GetFinalAnswer() ([]PathBetween, error) {
	usersInfo, err := GetUsersInfo()
	if err != nil {
		return nil, fmt.Errorf("can't get users %s", err)
	}

	pairs, err := GetRequiredPairs()
	if err != nil {
		return nil, fmt.Errorf("can't get pairs %s", err)
	}

	var finalAnswer []PathBetween

	var path []Subscriber

	parents := CreateParentsMap(usersInfo)
	subscribers := CreateSubscribersMap(usersInfo)

	for count, emails := range pairs {
		if CheckforSub(emails[0], emails[1], usersInfo) || emails[0] == emails[1] {
			path = nil
		} else {
			path = SearchPath(emails[0], emails[1], parents, subscribers)
		}

		ID := count + 1 // nolint: gomnd
		pathBetweenTwoUsers := PathBetween{ID: ID, InitEmail: emails[0], FinalEmail: emails[1], Path: path}

		finalAnswer = append(finalAnswer, pathBetweenTwoUsers)
	}

	return finalAnswer, nil
}
func GetUsersInfo() ([]User, error) {
	userFile, err := os.Open("users.json")
	if err != nil {
		return nil, fmt.Errorf("can't open users file %s", err)
	}
	defer userFile.Close()

	userDecoder := json.NewDecoder(userFile)

	var userList []User

	err = userDecoder.Decode(&userList)
	if err != nil {
		return nil, fmt.Errorf("can't start decoding json file %s", err)
	}

	return userList, nil
}

func GetRequiredPairs() ([][]string, error) {
	inputFile, err := os.Open("input.csv")
	if err != nil {
		return nil, fmt.Errorf("can't open input file %s", err)
	}

	defer inputFile.Close()

	pairsReader := csv.NewReader(inputFile)

	pairsList, err := pairsReader.ReadAll()
	if err != nil {
		return nil, fmt.Errorf("can't read all pairs of emails from file %s", err)
	}

	return pairsList, nil
}

func CreateParentsMap(users []User) map[string][]string {
	parents := make(map[string][]string)

	for _, userName := range users {
		var parentsList []string

		for _, userParent := range users {
			for _, userParentSubs := range userParent.Subscribers {
				if userName.Email == userParentSubs.Email && userName.CreatedAt == userParentSubs.CreatedAt {
					parentsList = append(parentsList, userParent.Email)
				}
			}
		}

		parents[userName.Email] = parentsList
	}

	return parents
}

func CreateSubscribersMap(users []User) map[string]string {
	usersMap := make(map[string]string)

	for _, user := range users {
		usersMap[user.Email] = user.CreatedAt
	}

	return usersMap
}

func CheckforSub(userEmail string, userToCheckInEmail string, users []User) bool {
	for _, user := range users {
		if userToCheckInEmail == user.Email {
			for _, subMembers := range user.Subscribers {
				if subMembers.Email == userEmail {
					return true
				}
			}

			break
		}
	}

	return false
}

func SearchPath(initObj, finalObj string, parents map[string][]string, subscribers map[string]string) []Subscriber {
	currentChild, finishedEmail := FindPath(parents, initObj, finalObj)
	if finishedEmail == "" {
		return nil
	}

	path := RecreatePath(currentChild, subscribers, initObj, finishedEmail)

	return path
}

func RecreatePath(currentChild, subscribers map[string]string, initObj, finishedEmail string) []Subscriber {
	var path []Subscriber

	for {
		if finishedEmail == initObj {
			break
		}

		path = append(path, Subscriber{Email: finishedEmail, CreatedAt: subscribers[finishedEmail]})
		finishedEmail = currentChild[finishedEmail]
	}

	correctPath := reversePath(path)

	return correctPath
}

func FindPath(parentsMap map[string][]string, initObj, finalObj string) (map[string]string, string) {
	usedEmail := make(map[string]bool)
	currentChild := make(map[string]string)

	var finishedEmail string

	userQueue := []string{initObj}

	for {
		if len(userQueue) == 0 {
			break
		}

		user := userQueue[0]
		userQueue = append(userQueue[:0], userQueue[1:]...)

		if user == finalObj {
			finishedEmail = currentChild[user]
			break
		}

		for _, parents := range parentsMap[user] {
			if !usedEmail[parents] {
				usedEmail[parents] = true
				currentChild[parents] = user

				userQueue = append(userQueue, parents)
			}
		}
	}

	return currentChild, finishedEmail
}

// nolint: gomnd
func reversePath(path []Subscriber) []Subscriber {
	revPath := make([]Subscriber, len(path))
	j := len(path) - 1

	for _, user := range path {
		revPath[j] = user
		j--
	}

	return revPath
}
